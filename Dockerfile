FROM python:3.7
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT true
RUN mkdir /src
RUN mkdir /opt/conf
WORKDIR /src

ADD ./src /src
ADD gunicorn.py.ini /opt/conf/gunicorn.py.ini
#RUN apk add build-base
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
CMD gunicorn -c /opt/conf/gunicorn.py.ini distribution.wsgi
