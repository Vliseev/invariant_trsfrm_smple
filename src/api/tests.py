from django.test import TestCase
from prob_engine.invtransfmsampler import InvTransfmSampler
from numpy.random import standard_t
from scipy.stats import ks_2samp, chi2

# Create your tests here.
class SamplerTestCase(TestCase):

    def test_sample_t_distrib(self):
        sample_size = 1000
        cdf_module = "from scipy.stats import t\ndef cdf(x):\n    \n    return t._cdf(x,10)\n"
        cdf_name = "cdf"
        sampler = InvTransfmSampler(cdf_module,cdf_name,sample_size)
        s1 = sampler.sample_distribution()
        s2 = standard_t(10,size=sample_size)

        test_result = ks_2samp(s1,s2)
        self.assertGreater(test_result.pvalue,0.05)

    def test_sample_chi2_distrib(self):
        sample_size = 100
        cdf_module = "from scipy.stats import chi2\ndef cdf(x):\n    \n    return chi2.cdf(x,df=10)\n"
        cdf_name = "cdf"
        sampler = InvTransfmSampler(cdf_module,cdf_name,sample_size)
        s1 = sampler.sample_distribution()
        s2 = chi2.rvs(10,size=sample_size)

        test_result = ks_2samp(s1,s2)
        self.assertGreater(test_result.pvalue,0.05)

