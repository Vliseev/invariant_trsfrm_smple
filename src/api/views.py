from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from api.serrializers import RequestSerializer
import json

class Sampler(APIView):

    def post(self,reques):
        sampler_req = RequestSerializer(data=reques.data)
        if sampler_req.is_valid():
            sampler = sampler_req.create(reques.data)
            try:
                sample = sampler.sample_distribution()
            except Exception as err:
                return Response(status=status.HTTP_400_BAD_REQUEST,data=json.dumps({'err':err.__str__()}))
            request_sample = json.dumps({'sample':sample})
            return Response(status=status.HTTP_200_OK,data=request_sample)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
