from rest_framework import serializers
from prob_engine.invtransfmsampler import InvTransfmSampler

class RequestSerializer(serializers.Serializer):

    cdf_module = serializers.CharField()
    cdf_name = serializers.CharField()
    sample_size = serializers.IntegerField()

    def create(self, validated_data):
        return InvTransfmSampler(**validated_data)

    def update(self, instance, validated_data):
        for field, value in validated_data.items():
            setattr(instance, field, value)
        return instance