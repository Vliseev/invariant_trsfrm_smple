from scipy.optimize import root as _root
from numpy.random import uniform as _uniform

class InvTransfmSampler:
    """
    Sampling generator which using method Inverse transform
    see ```https://en.wikipedia.org/wiki/Inverse_transform_sampling```

    Attributes:
        cdf_module (str): module with given distribution function.
        cdf_name (str).
        sample_size (int)

    """
    def __init__(self, cdf_module: str, cdf_name: str, sample_size: int):
        self.cdf_module = cdf_module
        self.cdf_name = cdf_name
        self.sample_size = sample_size

    def _parse_function(self):
        code = compile(self.cdf_module, filename='', mode='exec')
        namespace = {}
        exec(code, namespace)
        return namespace[self.cdf_name]
            
    def sample_distribution(self):
        seed = _uniform(0, 1, self.sample_size)
        cdf_foo = self._parse_function()
        return [_root(lambda x: cdf_foo(x) - u,0).x[0] for u in seed]
